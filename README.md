# lifx web control
web based control for the lifx lamp

You can use touch gestures to control your lifx in a web-browser
* swipe left/right = change color
* swipe up / down = change power
* swipe with two fingers left = power to 0% :turing off
* swipe with two fingers right = power to 100% : turning on
* long tap = random color

Used languages:
* Python
* PHP
* Javascript / Jquery

Used libraries:
* https://github.com/davidmerfield/randomColor
* https://github.com/mattbryson/TouchSwipe-Jquery-Plugin
* https://github.com/mclarkk/lifxlan
* https://github.com/jquery/jquery


This project was created mainly for learning purposes.

How it works:

The javascript in the index.html calls light.php which is used as a bridge to the python api which is build on top of the lifxlan library
# How to install

Debian:

    sudo apt-get update; sudo apt-get install -y php5 python git python-pip; 
    sudo pip install lifxlan;
    git clone https://github.com/jan-loeffler/lifx_web_control.git;
    cd lifx_web_control/;
    php -S 0.0.0.0:1234;

Then go to http://[ip of the computer]:1234 and enjoy the app!

The lifx needs to be installed in the same network as the computer you run this code on

##TO DO:
* Init of the app with the color of the lamp
* Speed improvements
* Control for non touch devices
