#!/usr/bin/env python
from lifxlan import *
import sys

def modTupByIndex(tup, index, ins):
    return tuple(tup[0:index]) + (ins,) + tuple(tup[index+1:])

if (len(sys.argv) == 2 and (sys.argv[1] == '--help' or sys.argv[1] == '-h')):
	print 'Usage: lifx_api.py [color 0 - 360][sat 0 - 100] [power 0 - 100]'
	print 'or lifx_api.py --power [power 0 - 100]'
	print 'or lifx_api.py --g for printing the color of the light'
	sys.exit()

lifx = LifxLAN(1)
devices = lifx.get_lights()
bulb = devices[0]
old_color = bulb.get_color()

if len(sys.argv) == 1:
	print("Color: {}".format(bulb.get_color()))
	sys.exit()

if (len(sys.argv) == 2 and (sys.argv[1] == '--help' or sys.argv[1] == '-h')):
	print 'Usage: lifx_api.py [color 0 - 360][sat 0 - 100] [power 0 - 100]'
	sys.exit()


if (len(sys.argv) == 2 and (sys.argv[1] == '-gc' or sys.argv[1] == '--getcolor')):
	print("hsl("+"{},".format(int(bulb.get_color()[0]/182.041666667))+"{}".format(int(bulb.get_color()[1]/655.35))+'%,'+"{}".format(int(bulb.get_color()[2]/655.35))+'%)')
	#print("sat {}".format(int(bulb.get_color()[1]/655.35)))
	#print("power {}".format(int(bulb.get_color()[2]/655.35)))
	sys.exit()
#"hsl(83, 79.33%, 70.97%)"
if (len(sys.argv) == 2 and (sys.argv[1] == '-gc_hsl' or sys.argv[1] == '--getcolor_hsl')):
	print("color {}".format(int(bulb.get_color()[0]/182.041666667)))
	print("sat {}".format(int(bulb.get_color()[1]/655.35)))
	print("power {}".format(int(bulb.get_color()[2]/655.35)))
	sys.exit()

if (len(sys.argv) == 3 and (sys.argv[1] == '-p' or sys.argv[1] == '--power')):
	power = int(655.35*int(sys.argv[2]))
	new_color = old_color
	new_color = modTupByIndex(new_color,2,power)
	bulb.set_color(new_color,0,True)
	sys.exit()



if len(sys.argv) == 4 :
	color = int(182.041666667*int(sys.argv[1]))
	sat = int(655.35*int(sys.argv[2]))
	power = int(655.35*int(sys.argv[3]))
	new_color = old_color
	new_color = modTupByIndex(new_color,0,color)
	new_color = modTupByIndex(new_color,1,sat)
	new_color = modTupByIndex(new_color,2,power)
	bulb.set_color(new_color,0,True)
